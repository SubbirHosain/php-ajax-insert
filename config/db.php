<?php
//MySQL connection details.
$host = 'localhost';
$user = 'root';
$pass = '';
$database ='signup';


try {
	
	//Connect to MySQL and instantiate our PDO object.
	$dbh = new PDO("mysql:host=$host;dbname=$database",$user,$pass);
	// set the PDO error mode to exception
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
	//echo "Connected successfully";
	
}

catch(PDOException $e) {
	 echo "Connection failed: " . $e->getMessage();
}
?>