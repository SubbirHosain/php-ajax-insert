$(document).ready(function() {
	//creating the variables for fields
	//making variables empty where there is an error it's mandatory
    var fname = '';
    var lname = '';
    var uemail = '';
    var upassword = '';
    var repassword = '';
    var gender = '';

    $('#fname').keyup(function() {
      
        var vall = $(this).val();

        if (vall == '') {
            //$('#fnameError').html('<span style="color:red">First name required</span>');
            $('#fnameError').addClass('has-error');
            $('#fnameError').removeClass('has-success');
            fname = '';
        } else {
           // $('#fnameError').html('<span style="color:green">awesome</span>');
            $('#fnameError').removeClass('has-error');
            $('#fnameError').addClass('has-success');
            fname = vall;
        }
    });
    $('#lname').keyup(function() {
        /* Act on the event */
        var vall = $(this).val();

        if (vall == '') {
            $('#lnameError').html('Last name required');
            lname = '';
        } else {
            $('#lnameError').html('awesome');
            lname = vall;
        }
    });
    $('#uemail').keyup(function() {
        var vall = $(this).val();

        if(vall==''){
        	$('#uemailError').html('Enter your Email address');
        	uemail='';
        } 
        else {

        	$.ajax({

        		type:'post',
        		url:'process.php',
        		data:'uemail='+vall,
        		success:function(data){
        			if(data=='invalid'){
        				$('#uemailError').html('Email is invalid');
        				uemail='';
        			}
        			else if(data=='exsits'){
        				$('#uemailError').html('Email  already exists ');
        				uemail='';
        			}
        			else if (data == 'ok'){
        				$('#uemailError').html('awesome');
        				uemail=vall;
        			}
        		}

        	});

        }
    });

    $('#upassword').keyup(function() {
        /* Act on the event */
        var vall = $(this).val();
console.log(vall.length);
        if(vall =='')
        {
        	$('#passwordError').html('Enter your password');
        	upassword='';

        }
        else if(vall.length < 9) {
        	$('#passwordError').html('password must be greater than 8 characters');
        	upassword='';
        } 
        else{
        	$('#passwordError').html('awesome');
        	upassword = vall;
        }
    });
    $('#repassword').keyup(function() {
        /* Act on the event */
        var vall = $(this).val();

        if(upassword!=vall){
        	$('#repasswordError').html('did not match');
        	repassword='';
        }
        else {
        	$('#repasswordError').html('matched');
        	repassword=vall;
        }
    });

    $('#male').click(function() {
    	
    	gender ='Male';
    	$('#genderError').html('awesome');

    });

    $('#female').click(function() {
    	
    	gender ='Female';
    	$('#genderError').html('awesome');

    });    


    $('#signupBtn').click(function(event) {
    	event.preventDefault();
    	//var formDiv = $('#signupForm');
    	//formDiv.html('<img src="loading.gif" width="100" alt="">')
    	
    	if(fname == ''|| lname ==''||uemail ==''|| upassword =='' || repassword==''|| gender =='') {

    		$('#formError').html('plz correct the errors in the form');
    	} 
    	else{
    		$.ajax({
    			type:'post',
    			url:'process.php',
    			data:'fname='+fname+'&lname='+lname+'&uemail='+uemail+'&upassword='+upassword+'&gender='+gender,
    			success:function(data){
    				
    				if(data=='done'){
    					// $('#formError').html('You are signed up');
    					$('#signupForm').html('You are signed up');
    				}
    				else{
    					// $('#formError').html('ther is an error ,try again later');
    					$('#signupForm').html('You are signed up');
    				}
    			}

    		});
    	}
    });




});