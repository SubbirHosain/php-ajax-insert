<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>
    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="jumbotron">
            <div class="row">
                <div class="col-md-offset-3 col-md-6">

                    <form method="post" id="signupForm">
                        <div class="form-group" id="fnameError">
                            <label for="fnamef">First name</label>
                            <input type="text" class="form-control" value="" id="fname" >
                            <span class="errors" id="fnameError"></span>
                        </div>
                        <div class="form-group">
                            <label for="lname">Las name</label>
                            <input type="text" class="form-control" id="lname" >
                            <span class="errors" id="lnameError"></span>
                        </div>
                        <div class="form-group">
                            <label for="uemail">Email Address</label>
                            <input type="email" class="form-control" name="user_email" id="uemail" >
                            <span class="errors" id="uemailError"></span>
                        </div>
                        <div class="form-group">
                            <label for="upassword">Password</label>
                            <input type="password" class="form-control" id="upassword" >
                            <span class="errors" id="passwordError"></span>
                        </div>
                        <div class="form-group">
                            <label for="repassword">Confirm password</label>
                            <input type="password" class="form-control" id="repassword" >
                            <span class="errors" id="repasswordError"></span>
                        </div>
                        <div class="form-group">
                            <label class="radio-inline">
                                <input type="radio" name="gender" id="male" value="Male"> Male
                            </label>

                            <label class="radio-inline">
                                <input type="radio" name="gender" id="female" value="Female"> Female
                            </label>
                            <span class="errors" id="genderError"></span>
                        </div>
                        <button type="submit" id="signupBtn" class="btn btn-default">Submit</button>
                        <span class="errors" id="formError"></span>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/main.js"></script>
</body>

</html>